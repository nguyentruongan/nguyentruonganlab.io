---
title: 🔖 Tags
icon: material/tag
description: All categories of Nguyen Truong An water science website
template: tags.html
hide:
  - navigation
  - toc
---


<details>
<summary>Click to show/hide the linking notes graph</summary>

<iframe id="test"
        title='test'
        src="./assets/graph.html"
        class="graph"
        width="1400px"
        height="500px"
        allowtransparency="true"
        style="border: 0px; margin: 0px; padding: 0px; overflow: hidden;display: block; margin: auto auto;"
        scrolling="no">
</iframe>

</details>